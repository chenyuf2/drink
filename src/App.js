import "./App.css";
import drinkLogo from "./img/white_bg_logo.png";
import juice from "./img/juice.png";
import leaf1 from "./img/leaf01.png";
import leaf2 from "./img/leaf02.png";
import leaf3 from "./img/leaf03.png";
import leaf4 from "./img/leaf04.png";
import leaf5 from "./img/leaf05.png";
import React, { useRef, useEffect, useState } from "react";
import { Elastic, Back, Power2, Power3, Expo } from "gsap";
import { Tween, TweenMax, Controls, PlayState } from "react-gsap";
const App = () => {
  return (
    <div className="wrapper w-100">
      <div className="nav-header w-100 pl-5 pr-5">
        <Tween
          from={{
            opacity: 0,
            x: -20,
            ease: Expo.easeInOut,
          }}
        >
          <div className="drink-logo">
            <img src={drinkLogo}></img>
          </div>
        </Tween>

        <div className="menu-links p-0">
          <ul className="d-flex align-items-center m-0 p-0">
            <Tween
              from={{
                opacity: 0,
                x: -20,
                ease: Power3.easeInOut,
              }}
              stagger={0.08}
            >
              <li>locations</li>
              <li className="ml-4">our menu</li>
              <li className="ml-4">our story</li>
              <li className="ml-4">meet our team</li>
              <li className="ml-4">own a clean juice</li>
              <li className="ml-4">Contact US</li>
            </Tween>
          </ul>
        </div>

        <div className="search position-relative">
          <Tween
            from={{
              delay: 0.5,
              opacity: 0,
              x: -20,
              ease: Expo.easeInOut,
            }}
          >
            <input
              className=" font-weight-bold"
              placeholder="Search"
              aria-label="Search"
            />

            <div className="search-logo-container position-absolute d-flex align-items-center">
              <i className="fa fa-search"></i>
            </div>
          </Tween>
        </div>

        <div className="d-flex align-items-center">
          <Tween
            from={{
              delay: 0.6,
              opacity: 0,
              x: -20,
              ease: Expo.easeInOut,
            }}
          >
            <span className="account">my account</span>
          </Tween>
          <Tween
            from={{
              delay: 0.7,
              opacity: 0,
              x: -20,
              ease: Expo.easeInOut,
            }}
          >
            <div className="ml-3 position-relative">
              <i className="fa fa-shopping-bag  bag-logo"></i>
              <div className="text-white circle-container position-absolute d-flex align-items-center justify-content-center cart-num-position">
                13
              </div>
            </div>
          </Tween>
          <Tween
            from={{
              delay: 0.8,
              opacity: 0,
              x: -20,
              ease: Expo.easeInOut,
            }}
          >
            <div className="ml-4 bar-menu">
              <i className="fa fa-bars"></i>
            </div>
          </Tween>
        </div>
      </div>

      <div className="content d-flex align-items-center flex-column justify-content-center">
        <div className="d-flex justify-content-between align-items-center w-68">
          <Tween
            from={{
              delay: 1.3,
              opacity: 0,
              x: -20,
              ease: Expo.easeInOut,
            }}
          >
            <div className="tagline" style={{ fontWeight: "600" }}>
              Find your clean juice
            </div>
          </Tween>
          <Tween
            from={{
              delay: 1.3,
              opacity: 0,
              x: -20,
              ease: Expo.easeInOut,
            }}
          >
            <div className="pages">
              <span>2</span>/21
            </div>
          </Tween>
        </div>
        <div className="title-container d-flex align-items-center position-relative justify-content-between w-100 pl-5 pr-5">
          <Tween
            from={{
              delay: 2,
              opacity: 0,
              ease: Expo.easeInOut,
            }}
          >
            <div className="d-flex align-items-center">
              <button className="prev btn cursor-pointer">
                <div className="arrow d-flex align-items-center justify-content-center">
                  <i className="fa fa-chevron-left"></i>
                </div>
              </button>
              <div className="font-weight-bold arrow-hint">Previous</div>
            </div>
          </Tween>
          <Tween
            from={{
              delay: 1,
              opacity: 0,
              y: 20,
              ease: Expo.easeInOut,
            }}
          >
            <div className="title" style={{ lineHeight: "0.9" }}>
              orange
            </div>
          </Tween>
          <Tween
            from={{
              delay: 2,
              opacity: 0,
              ease: Expo.easeInOut,
            }}
          >
            <div className="d-flex align-items-center">
              <div className="font-weight-bold arrow-hint">Next</div>
              <button className="next btn cursor-pointer">
                <div className="arrow d-flex align-items-center justify-content-center">
                  <i className="fa fa-chevron-right"></i>
                </div>
              </button>
            </div>
          </Tween>
        </div>
        <div className="small-size-window w-68">
          <div className="d-flex position-relative juice-container mt-4">
            <Tween
              from={{
                delay: 2,
                opacity: 0,
                y: -800,
                ease: Expo.easeInOut,
              }}
            >
              <div className="small-juice-logo w-50 d-flex justify-content-center align-items-center">
                <img src={juice} alt="juice logo" />
              </div>
            </Tween>
            <div className="d-flex align-items-center justify-content-center w-50 flex-column sub-title-container">
              <div className="more">
                <Tween
                  from={{
                    delay: 1.4,
                    opacity: 0,
                    y: 20,
                    ease: Expo.easeInOut,
                  }}
                >
                  <button className="btn pl-4 pr-4 pt-3 pb-3" href="#">
                    <span>show all the juices</span>
                  </button>
                </Tween>
              </div>

              <Tween
                from={{
                  delay: 1.5,
                  opacity: 0,
                  y: 20,
                  ease: Expo.easeInOut,
                }}
              >
                <div className="desc mt-3">
                  <p className="line-height-2">
                    Your <span>healthy</span> life <br></br>
                    starts here with us
                  </p>
                  <p className="line-height-7 mb-0">
                    A family owned company founded with the
                  </p>
                  <p className="line-height-7 mb-0">
                    purpose of giving your family access to
                  </p>
                  <p className="line-height-7 mb-0">
                    clean, organic products while you are on the go.
                  </p>
                </div>
              </Tween>
            </div>
          </div>
        </div>
        <div className="large-size-window w-68">
          <div className="d-flex mt-5 justify-content-between ">
            <Tween
              from={{
                delay: 1.4,
                opacity: 0,
                y: 20,
                ease: Expo.easeInOut,
              }}
            >
              <div className="more">
                <button className="btn" href="#">
                  <span>show all the juices</span>
                  <i className="fa fa-bars fa-line-columns ml-5 text-white"></i>
                </button>
              </div>
            </Tween>
            <Tween
              from={{
                delay: 1.4,
                opacity: 0,
                y: 20,
                ease: Expo.easeInOut,
              }}
            >
              <div className="desc">
                <p className="line-height-2">
                  Your <span>healthy</span> life <br></br>
                  starts here with us
                </p>
                <p className="line-height-7 mb-0">
                  A family owned company founded with the
                </p>
                <p className="line-height-7 mb-0">
                  purpose of giving your family access to clean,
                </p>
                <p className="line-height-7 mb-0">
                  organic products while you are on the go.
                </p>
              </div>
            </Tween>
          </div>
        </div>

        <div className="juice large-size-window">
          <Tween
            from={{
              delay: 2,
              opacity: 0,
              y: -800,
              ease: Expo.easeInOut,
            }}
          >
            <img src={juice} alt="juice logo" />
          </Tween>
        </div>
      </div>
      <div className="leaves">
        <ul id="scene">
          <Tween
            from={{
              delay: 2.1,
              opacity: 0,
              y: -800,
              ease: Expo.easeInOut,
            }}
          >
            <li className="layer" data-depth="-.1">
              <img src={leaf1} alt="" />
            </li>
          </Tween>
          <Tween
            from={{
              delay: 2.2,
              opacity: 0,
              y: -800,
              ease: Expo.easeInOut,
            }}
          >
            <li className="layer" data-depth="-.3">
              <img src={leaf2} alt="" />
            </li>
          </Tween>
          <Tween
            from={{
              delay: 2.3,
              opacity: 0,
              y: -800,
              ease: Expo.easeInOut,
            }}
          >
            <li className="layer" data-depth="-1.5">
              <img src={leaf3} alt="" />
            </li>
          </Tween>
          <Tween
            from={{
              delay: 2.4,
              opacity: 0,
              y: -800,
              ease: Expo.easeInOut,
            }}
          >
            <li className="layer" data-depth=".1">
              <img src={leaf4} alt="" />
            </li>
          </Tween>
          <Tween
            from={{
              delay: 2.5,
              opacity: 0,
              y: -800,
              ease: Expo.easeInOut,
            }}
          >
            <li className="layer" data-depth=".3">
              <img src={leaf5} alt="" />
            </li>
          </Tween>
        </ul>
      </div>
    </div>
  );
};

export default App;
